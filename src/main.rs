use clap::{clap_app, ArgMatches};
use log::{error, info, LevelFilter};
use std::fs;

mod runner;

#[macro_use]
extern crate lazy_static;

fn main() {
    // Clap App
    let matches = clap_app!(AutoBench =>
        (version: "0.1.0")
        (author: "Joseph Skubal")
        (about: "Performs automated benchmarks and generates charts from the results")
        (@arg verbose: -v --verbose "Logs additional information to the console")
        (@arg quiet: -q --quiet "Only errors are logged to the console")
        (@arg FILE: "The benchmark file that should be executed. Defaults to 'benchmark.toml'")
        (@subcommand template =>
            (about: "Generates a template 'benchmark.toml' file")
        )
    )
    .get_matches();

    configure_logger(&matches);

    // Start
    if let Some(subcommand) = matches.subcommand_name() {
        match subcommand {
            "template" => make_template(),
            _ => unreachable!(),
        }
    } else {
        let file = matches.value_of("FILE").unwrap_or("benchmark.toml");
        if let Err(e) = runner::run(file) {
            handle_runner_error(e);
        }
    }
}

/// Configures the logger that is used for this application
fn configure_logger(matches: &ArgMatches) {
    let level_filter = if matches.is_present("verbose") {
        LevelFilter::Debug
    } else if matches.is_present("quiet") {
        LevelFilter::Error
    } else {
        LevelFilter::Info
    };
    env_logger::Builder::new()
        .format_timestamp(None)
        .format_module_path(false)
        .format_level(false)
        .filter(None, level_filter)
        .init();
}

/// Handles errors in the runner module
fn handle_runner_error(e: runner::ErrorType) {
    match e {
        runner::ErrorType::InvalidFile => error!("Specified benchmark file is invalid!"),
        runner::ErrorType::RunAborted(x) => {
            error!("Run aborted due to extreme circumstances: {}", x)
        }
        runner::ErrorType::SaveResultsFailure => {
            error!("An error occurred while trying to save benchmark results.")
        }
    }
}

/// Saves the template file to the current working directory
fn make_template() {
    match fs::write("benchmark.toml", include_str!("template.toml")) {
        Ok(_) => info!("Saved template as benchmark.toml"),
        Err(_) => error!("Unable to save template"),
    }
}
