//! This module handles running the benchmarks and saving the results.
//!
//! The runner performs a series of actions in order.
//!
//! ## 1. Parse File
//! Next, the file will be read by the system. If the file fails to be read, then the runner will
//! exit.
//!
//! ## 2. Execute Benchmarks
//! Next, the file will use the data from the file to execute the benchmarks. If something goes
//! wrong during the benchmarks, the runner will exit.
//!
//! ## 3. Save Data
//! Finally, benchmark data will be saved to a `.csv` file.

use benchmark::{BenchmarkJob, BenchmarkResult};
use log::{debug, info, warn};
use specification::Specification;
use std::fs;
use std::iter;

mod benchmark;
mod specification;

/// An enum that describes the different errors that can occur
#[derive(Copy, Clone, Debug)]
pub enum ErrorType {
    /// Indicates that benchmarking started, but was unable to complete successfully
    RunAborted(&'static str),
    /// Indicates that the benchmark file was invalid
    InvalidFile,
    /// Indicates that the program failed while trying to save the results
    SaveResultsFailure,
}

/// The function to start the runner.
///
/// If the runner stops executing becuase something went wrong, an `Err<ErrorType>` will be returned.
pub fn run(file_name: &str) -> Result<(), ErrorType> {
    let toml_data = fs::read_to_string(file_name).map_err(|_| ErrorType::InvalidFile)?;
    debug!("File {} read successfully", file_name);

    let specification: Specification =
        toml::from_str(&toml_data).map_err(|_| ErrorType::InvalidFile)?;

    let mut programs = specification.get_programs();
    let num_programs = programs.len();
    debug!("Discovered {} benmarks", num_programs);

    let mut count = 1;
    for program in programs.iter_mut() {
        info!(
            "Benchmarking program: {} [{}/{}]",
            program.name(),
            count,
            num_programs
        );
        match program
            .execute(specification.get_timeout())
            .map_err(|e| ErrorType::RunAborted(e))?
        {
            BenchmarkResult::Complete(status) => debug!(
                "Benchmark Complete with exit code: {}",
                status
                    .code()
                    .map(|x| x.to_string())
                    .unwrap_or_default()
            ),
            BenchmarkResult::TimedOut => warn!("Timeout reached! Benchmark Stopped"),
        };
        count += 1;
    }

    let results = iter::once(BenchmarkJob::to_csv_heading())
        .chain(programs.iter().map(|job| job.to_csv_row()))
        .collect::<Vec<String>>()
        .join("\n");

    fs::write("AutoBench-results.csv", results).map_err(|_| ErrorType::SaveResultsFailure)?;
    info!("Benchmarks Complete! Results saved to `AutoBench-results.csv`");
    Ok(())
}
