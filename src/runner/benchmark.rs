//! This module contains the implementations required to actually conduct the benchmarks.
//!
//! The [`BenchmarkJob`] struct provides the interface from which jobs can be run and exported to
//! csv files.

use std::io::Write;
use std::process::{Command, ExitStatus, Stdio};
use std::time::{Duration, Instant};
use wait_timeout::ChildExt;

/// The result of executing a [`BenchmarkJob`]
#[derive(Debug, PartialEq)]
pub enum BenchmarkResult {
    /// Shows that the job completed on its own. Returns with the exit result
    Complete(ExitStatus),
    /// Shows that the job had to be killed due to timing out.
    TimedOut,
}

/// The interface to running a single benchmark
///
/// BenchmarkJob objects know how to be run as child processes, and how to convert their data to
/// csv.
#[derive(Debug, PartialEq)]
pub struct BenchmarkJob {
    name: String,
    command: String,
    command_args: Vec<String>,
    args: Vec<String>,
    input: String,
    duration: Option<Duration>,
}

impl BenchmarkJob {
    /// Returns a new BenchmarkJob with the specified attributes
    pub fn new<T>(name: &str, command: &str, command_args: T, args: T, input: &str) -> Self
    where
        T: IntoIterator,
        T::Item: ToString,
    {
        Self {
            name: name.into(),
            command: command.into(),
            command_args: command_args.into_iter().map(|x| x.to_string()).collect(),
            args: args.into_iter().map(|x| x.to_string()).collect(),
            input: input.into(),
            duration: None,
        }
    }

    /// Returns an immutable reference to the name of this program
    pub fn name(&self) -> &String {
        &self.name
    }

    /// Returns the header for a CSV file of BenchmarkJob entries
    ///
    /// ```
    /// assert_equal(BenchmarkJob.to_csv_heading(), "name,command,args,input,duration".into())
    /// ```
    pub fn to_csv_heading() -> String {
        String::from("name,command,args,input,duration")
    }

    /// Serializes the details of this BenchmarkJob to a CSV row
    pub fn to_csv_row(&self) -> String {
        let command = if !self.command_args.is_empty() {
            Some(format!("{} {}", self.command, self.command_args.join(" ")))
        } else {
            None
        };

        format!(
            "{},{},{},{},{}",
            self.name,
            command.as_ref().unwrap_or(&self.command),
            self.args.join(" "),
            self.input,
            self.duration
                .map(|d| d.as_secs_f64().to_string())
                .unwrap_or_default()
        )
    }

    /// Executes this `BenchmarkJob`, spawning a child process and recording how long it takes to
    /// terminate.
    pub fn execute(&mut self, timeout: Duration) -> Result<BenchmarkResult, &'static str> {
        // Build the child process
        let mut builder = Command::new(&self.command);

        // Add Arguments
        for arg in self.command_args.iter().chain(self.args.iter()) {
            builder.arg(arg);
        }

        // Attach pipes for IO
        builder.stdin(Stdio::piped()).stdout(Stdio::null());

        // Log the start time and spawn the child
        let start = Instant::now();
        let mut child = builder
            .spawn()
            .map_err(|_| "Unable to start child process")?;

        // Feed in the inputs
        child
            .stdin
            .as_mut()
            .ok_or("Failed to open pipe to child process")?
            .write_all(self.input.as_bytes())
            .map_err(|_| "Failed to write into pipe to child process")?;

        // Wait for the child to stop (or timeout)
        let exit_status = match child.wait_timeout(timeout) {
            Ok(Some(status)) => status,
            Err(_) => return Err("Error Polling Child"),
            Ok(None) => {
                child.kill().map_err(|_| "Unable to kill expired child")?;
                child.wait().map_err(|_| "Unable to reap expired child")?;
                return Ok(BenchmarkResult::TimedOut);
            }
        };

        // Stop the clock
        self.duration = Some(start.elapsed());
        Ok(BenchmarkResult::Complete(exit_status))
    }
}
