//! Contains the definition of the benchmark specification.
//!
//! The benchmark specification is laid out in the benchmark toml file. The struct in this module
//! implements [`serde::Deserialize`] allowing it to be converted to

use super::benchmark::BenchmarkJob;
use regex::Regex;
use serde::Deserialize;
use std::collections::HashMap;
use std::time::Duration;

lazy_static! {
    /// A Regular expression that handles breaking command line arguments into tokens.
    ///
    /// This is mostly splitting on whitespace, but elements surrounded with single or double
    /// quotes are left grouped together. This regex also supports escaping quotes with `\`
    static ref ARGUMENT_REGEX: Regex = Regex::new(r#""(\\"|[^"])*"|'(\\'|[^'])*'|\S+"#).unwrap();
}

/// Parses a string of arguments into an iterator over the arguments
fn parse_arguments(args: &str) -> impl Iterator<Item = String> + '_ {
    ARGUMENT_REGEX
        .captures_iter(args)
        .map(|capture| String::from(&capture[0]))
}

/// Represents a benchmark specification as read from a file
#[derive(Deserialize, Debug, PartialEq)]
pub struct Specification {
    metadata: HashMap<String, String>,
    #[serde(default)]
    global: Global,
    programs: Vec<Program>,
}

impl Specification {
    /// Returns a `Vec` of all the `BenchmarkJob` instances that need to be created
    pub fn get_programs(&self) -> Vec<BenchmarkJob> {
        let default_empty = vec![String::new()];
        let mut jobs = Vec::new();

        for program in &self.programs {
            let args = program
                .args
                .as_ref()
                .or_else(|| self.global.args.as_ref())
                .unwrap_or(&default_empty);

            for arg_set in args {
                let inputs = program
                    .inputs
                    .as_ref()
                    .or_else(|| self.global.inputs.as_ref())
                    .unwrap_or(&default_empty);

                for input in inputs {
                    jobs.push(self.generate_job(program, input, arg_set));
                }
            }
        }
        jobs
    }

    /// Converts a [`Program`] and input into a [`BenchmarkJob`](super::BenchmarkJob)
    fn generate_job(&self, program: &Program, input: &str, args: &str) -> BenchmarkJob {
        let mut command_args = parse_arguments(&program.command);
        BenchmarkJob::new(
            &program.name,
            &command_args.next().unwrap(),
            command_args,
            parse_arguments(args),
            input,
        )
    }

    /// Returns the timeout as specified in the benchmark file
    pub fn get_timeout(&self) -> Duration {
        Duration::from_secs_f64(self.global.timeout)
    }

    /// Returns a reference to the metadata hash for this file
    pub fn _get_metadata(&self) -> &HashMap<String, String> {
        &self.metadata
    }
}

/// Holds the global options specified in the benchmark file
#[derive(Deserialize, Debug, PartialEq)]
struct Global {
    pub inputs: Option<Vec<String>>,
    pub args: Option<Vec<String>>,
    pub timeout: f64,
}

impl Default for Global {
    fn default() -> Self {
        Global {
            inputs: None,
            args: None,
            timeout: 60.0,
        }
    }
}

/// Represents a single program that can be run as part of the benchmarking process
#[derive(Deserialize, Debug, PartialEq)]
struct Program {
    pub name: String,
    pub command: String,
    pub inputs: Option<Vec<String>>,
    pub args: Option<Vec<String>>,
}
