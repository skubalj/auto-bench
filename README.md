AutoBench: Automatic Benchmarking
=================================

AutoBench is an application that allows for multiple applications or scripts to be benchmarked in
a single batch job. Results are output to a CSV file

## Use
AutoBench uses TOML files to define the benchmark specification. By default, AutoBench looks for a 
file called `benchmark.toml`, however other files can be used by passing the file name to the 
program:
```bash
$ auto-bench other-benchmarks.toml
```

AutoBench can generate a template benchmark specification for you. This will save a new 
`benchmark.toml` file to the current working directory. This can be done using the command:
```bash
$ auto-bench template
```

## Template File

Benchmarks are specified using a template file. Template files are made up of three main sections.
1. `metadata` -- Information about the benchmarks, not currently used by the program
2. `global` -- Describes global fallbacks, and the timeout. If inputs or arguments are specified
   are not specified in each program's benchmark section, then the global values will be used
3. `programs` -- These sections contain the name of the program and the command used to execute it.
   In addition, inputs and arguments can be added to override the global values. 

Each combination of entries from the arguments and inputs lists is run as its own benchmark case. 

```toml
[metadata]
author = "Joseph Skubal"
date = "2021-03-29"

[global]
inputs = ["1000", "5000", "10000"]
timeout = 300

[[programs]]
name = "Python"
command = "python primes.py"

[[programs]]
name = "R"
command = "Rscript primes.R"
inputs = [""]
args = ["1000", "5000", "10000"]

[[programs]]
name = "Ruby"
command = "ruby primes.rb"
inputs = ["1000", "5000", "10000", "15000", "20000"]

# Add additional [[programs]] sections here for more benchmarks
```

## Acknowledgments
This application is made possible thanks to the following great projects:

* [clap](https://crates.io/crates/clap) -- Kevin K. and Contributors
* [env_logger](https://crates.io/crates/env_logger) -- The Rust Project Developers
* [lazy_static](https://crates.io/crates/lazy_static) -- The Rust Project Developers
* [log](https://crates.io/crates/log) -- The Rust Project Developers
* [regex](https://crates.io/crates/regex) -- The Rust Project Developers
* [Rust](https://rust-lang.org) -- The Rust Project Developers
* [serde](https://crates.io/crates/serde) -- David Tolnay, Erick Tryzelaar, Contributers
* [toml](https://crates.io/crates/toml) -- Alex Crichton

## License
This project is licensed under the terms of the MIT license. This provides you the freedom to do
almost anything with this project. 

Copyright (C) 2021 Joseph Skubal